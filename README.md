# Assignment 4 - Pokemon Trainer


## Deployment:
Deployed on Heroku

[Nicole's](https://pacific-meadow-29559.herokuapp.com/) - URL:  <https://pacific-meadow-29559.herokuapp.com/>

[Camilla's](https://aqueous-mesa-05590.herokuapp.com/) - URL:  <https://aqueous-mesa-05590.herokuapp.com/>

## Source:

<https://gitlab.com/nicben/pokemon-assignment/>


## Component Tree

Link to [component tree](https://gitlab.com/nicben/pokemon-assignment/-/blob/master/src/assets/Component%20tree.pdf).


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.7.

