import {Component} from '@angular/core'
import {SessionService} from "../../services/session.service";
import {Trainer} from "../../models/trainer.model";
import {Pokemon} from "../../models/pokemon.model";
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector:'app-pokemon-catalogue',
  template:
    `<div class="catalogue">
      <h1 class="title">Pokemon catalogue</h1>
      <app-pokemon-list [pokemons]="pokemons" [show]="false"></app-pokemon-list>
    </div>
    `
})
export class PokemonCataloguePage{
  constructor(private readonly sessionService: SessionService,
              private readonly pokemonService: PokemonService) {
  }

  get pokemons(): Pokemon[] {
    return this.pokemonService.pokemons
  }

  get trainer(): Trainer | undefined{
    return this.sessionService.trainer
  }

}
