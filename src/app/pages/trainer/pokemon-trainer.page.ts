import {Component} from '@angular/core'
import {Trainer} from "../../models/trainer.model";
import {SessionService} from "../../services/session.service";
import {Pokemon} from "../../models/pokemon.model";

@Component({
  selector:'app-pokemon-catalogue',
  template:
    `<div>
      <h1 class="title">My Pokemon's</h1>
      <div *ngIf="trainer">
        <app-pokemon-list [pokemons]="trainer.pokemon" [show]="true"></app-pokemon-list>
      </div>
    </div>`
})
export class PokemonTrainerPage {
  constructor(private readonly sessionService: SessionService) {
  }

  get trainer(): Trainer | undefined {
    return this.sessionService.trainer
  }
}
