import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {SessionService} from "../../services/session.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{
  constructor(
    private readonly router: Router,
    private readonly sessionService: SessionService,
  ) {}

  ngOnInit() {
    if (this.sessionService.trainer !== undefined) {
      this.router.navigate(['catalogue'])
    }
  }
}
