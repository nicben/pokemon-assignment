import { Injectable } from "@angular/core";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Trainer} from "../models/trainer.model";
import {Observable, of} from "rxjs";
import {finalize, retry, switchMap, tap} from "rxjs/operators";
import {SessionService} from "./session.service";
import {Pokemon} from "../models/pokemon.model";

const API_URL = environment.nicoleBaseUrl;
const API_TOKEN = environment.token;

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  public attempting: boolean = false;
  public error: string = '';

  constructor(private readonly http: HttpClient ,
              private readonly sessionService: SessionService) {
  }

  private findByUsername(username: string): Observable<Trainer[]>{
    return this.http.get<Trainer[]>(`${API_URL}/trainers?username=${username}`)
  }

  private createTrainer(username: string): Observable<Trainer>{
    const headers = {
      headers: new HttpHeaders({
        "X-API-Key": API_TOKEN,
        "Content-Type": "application/json"
      })
    };
    const body = {
        username: username,
        pokemon: []
      };

    return this.http.post<Trainer>(`${API_URL}/trainers`,body,headers);
  }

  public authenticate(username: string, onSuccess: () => void): void{
    this.attempting = true;

    this.findByUsername(username)
      .pipe(
        retry(3),
        switchMap((trainers: Trainer[]) => {
          if(trainers.length){
            return of(trainers[0])
          }
          return this.createTrainer(username)
        }),
        finalize(() => {
          this.attempting = false;
        }))
      .subscribe(
        (trainer: Trainer) => {
          if(trainer.id){
            this.sessionService.setTrainer(trainer)
            onSuccess();
          }
        },
        (error: HttpErrorResponse) => {
          this.error = error.message;
        }
      )
  }

  public collectPokemon(userId: number, pokemon: Pokemon): void {
    const headers = {
      headers: new HttpHeaders({
        "X-API-Key": API_TOKEN,
        "Content-Type": "application/json"
      })
    };

    this.http.patch(`${API_URL}/trainers/${userId}`, {
      pokemon:[...this.sessionService.trainer!.pokemon, pokemon]}, headers)
      .subscribe(response => {
        console.log(response);
        this.sessionService.addPokemon(pokemon)
      },
        (error: HttpErrorResponse) => {
          this.error = error.message;
        });
  }

  public removePokemon(userId: number, pokemon: Pokemon): void{
    const headers = {
      headers: new HttpHeaders({
        "X-API-Key": API_TOKEN,
        "Content-Type": "application/json"
      })
    };
    const pokemons: Pokemon[] = this.sessionService.trainer!.pokemon;
    pokemons!.forEach((element,index)=>{
      if(element.id === pokemon.id) pokemons!.splice(index,1);
    });

    this.sessionService.removePokemon(pokemons)

    this.http.patch(`${API_URL}/trainers/${userId}`, {
      pokemon:pokemons}, headers)
      .subscribe(response => {
          console.log(response);
        },
        (error: HttpErrorResponse) => {
          this.error = error.message;
        });
  }
}
