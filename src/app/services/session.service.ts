import {Injectable} from "@angular/core";
import {Trainer} from "../models/trainer.model";
import {Pokemon} from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _trainer: Trainer | undefined;

  constructor() {
    const storedTrainer = localStorage.getItem('trainer')
    if (storedTrainer) {
      this._trainer = JSON.parse(storedTrainer) as Trainer
    }
  }

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  setTrainer(trainer: Trainer): void {
    this._trainer = trainer;
    localStorage.setItem('trainer', JSON.stringify(trainer))
  }

  addPokemon(pokemon: Pokemon): void {
    this._trainer!.pokemon.push(pokemon)
    localStorage.setItem('trainer', JSON.stringify(this._trainer))
  }

  removePokemon(updatedPokemonList: Pokemon[]): void {
    this._trainer!.pokemon = updatedPokemonList;
    localStorage.setItem('trainer', JSON.stringify(this._trainer))
  }

  logout() {
    this._trainer = undefined;
    localStorage.removeItem('trainer')
  }
}
