import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Pokemon } from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private _baseURL: string = "https://pokeapi.co/api/v2/pokemon";
  private _githubImageURL: string = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/";
  private _pokemons: Pokemon[] = [];
  private _id: string | undefined = '';
  private _url: string = '';
  private _imageURL: string = '';
  private _error: string = '';
  public currentPageUrl = this._baseURL;
  public nextPageUrl: string = '';
  public prevPageUrl: string = '';
  public totalPokemons: number = 0;
  public baseNumPokemons: number = 0;

  constructor(private readonly http: HttpClient) {
  }

  public fetchPokemons() : void {
    this._pokemons = [];
    this.http.get<Pokemon[]>(`${this.currentPageUrl}`)
      .subscribe(((response: any) => {
          this.totalPokemons = response.count;
          this.nextPageUrl = response.next;
          this.prevPageUrl = response.previous;
          response.results.forEach((pokemon: Pokemon) => {
            this._url = pokemon.url;
            const newPokemon = this.addAttributes(pokemon)
            this._pokemons.push(newPokemon)
          });
          this.baseNumPokemons = this._pokemons.length
        })
        , (error: HttpErrorResponse) => {
          this._error = error.message;
        });
  }

  private id(url: string) {
    let arr = url.split('/');
    this._id = arr[arr.length - 2]
    return this._id
  }

  private imageUrl(): string {
    return this._imageURL = this._githubImageURL + this._id + ".svg";
  }

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  public error(): string {
    return this._error;
  }

  private addAttributes(pokemon: Pokemon): Pokemon {
    pokemon.id = this.id(pokemon.url)
    pokemon.imageURL = this.imageUrl()
    return pokemon;
  }

  public goToNextPage() {
    this.currentPageUrl = this.nextPageUrl;
    this.fetchPokemons()
  }

  public goToPrevPage() {
    this.currentPageUrl = this.prevPageUrl;
    this.fetchPokemons()
  }

  public goToFirstPage() {
    this.currentPageUrl = this._baseURL;
    this.fetchPokemons()
  }

}
