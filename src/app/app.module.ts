import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';

import { LoginPage } from "./pages/login/login.page";
import { PokemonTrainerPage } from "./pages/trainer/pokemon-trainer.page";
import { PokemonCataloguePage } from "./pages/catalogue/pokemon-catalogue.page";
import { NotFoundPage} from "./pages/not-found/not-found.page";

import { AppComponent } from './app.component';
import { PokemonListComponent } from "./components/pokemon-list/pokemon-list.component";
import { PokemonListItemComponent } from "./components/pokemon-list-item/pokemon-list-item.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { CardComponent } from "./components/card/card.component";
import { InputfieldComponent } from "./components/input-field/inputfield.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { PaginationComponent } from "./components/pagination/pagination.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    PokemonTrainerPage,
    PokemonCataloguePage,
    NotFoundPage,
    NavbarComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    PaginationComponent,
    CardComponent,
    InputfieldComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
