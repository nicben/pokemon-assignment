import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginPage} from "./pages/login/login.page";
import {PokemonCataloguePage} from "./pages/catalogue/pokemon-catalogue.page";
import {PokemonTrainerPage} from "./pages/trainer/pokemon-trainer.page";
import {AuthGuard} from "./services/auth.guard";
import {NotFoundPage} from "./pages/not-found/not-found.page";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'trainer',
    component: PokemonTrainerPage,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'catalogue',
    component: PokemonCataloguePage,
    canActivate: [ AuthGuard ]
  },
  {
    path: '**',
    component: NotFoundPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
