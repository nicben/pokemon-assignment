import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {TrainerService} from "../../services/trainer.service";

@Component({
  selector: 'app-input-field',
  templateUrl: 'inputfield.component.html',
  styleUrls: ['inputfield.component.css']
})
export class InputfieldComponent{
  constructor(
    private readonly router: Router,
    private readonly trainerService: TrainerService
  ) {}

  get attempting(): boolean {
    return this.trainerService.attempting;
  }

  onSubmit(username: string): void {
    this.trainerService.authenticate(username, async () => {
      await this.router.navigate([`catalogue`])
    })
  }
}
