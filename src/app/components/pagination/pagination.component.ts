import {Component, AfterContentChecked} from '@angular/core'
import { PokemonService } from "../../services/pokemon.service";

@Component({
  selector:'app-pagination',
  template:
    `
      <div class="pagination ">
        <div class="buttons">
          <button (click)=goToFirstPage()>
            <span class="material-icons inline-icon">
                first_page
            </span>
            First
          </button>
          <button (click)=goToPrevPage()>
          <span class="material-icons inline-icon">
            navigate_before
        </span>
            Prev
          </button>
          <button (click)=goToNextPage()>
            Next
            <span class="material-icons inline-icon">
            navigate_next
        </span>
          </button>
        </div>
        <div>
          <p> {{ sumPrev }} of {{ totalPokemons }}</p>
        </div>
      </div>
    `,
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements AfterContentChecked{
  totalPokemons = 0
  sumPrev = 0

  constructor(private readonly pokemonService: PokemonService) {
  }

  ngAfterContentChecked() {
    if(this.totalPokemons === 0) {
      this.totalPokemons = this.pokemonService.totalPokemons
      this.sumPrev = this.pokemonService.baseNumPokemons
    }
  }

  goToNextPage(): void {
    this.pokemonService.goToNextPage()
    this.sumPrev = this.sumPrev + this.pokemonService.baseNumPokemons
  }

  goToPrevPage(): void {
    this.pokemonService.goToPrevPage()
    this.sumPrev = this.sumPrev - this.pokemonService.baseNumPokemons
  }

  goToFirstPage(): void {
    this.pokemonService.goToFirstPage()
    this.sumPrev =  this.pokemonService.baseNumPokemons
  }

}
