import { Component, Input, OnInit } from '@angular/core'
import { PokemonService } from "../../services/pokemon.service";
import { Pokemon } from "../../models/pokemon.model";

@Component({
  selector:'app-pokemon-list',
  template:
    `<div class="container">
      <div class="pokemon-list">
        <div  *ngFor="let pokemon of pokemons ">
          <app-pokemon-list-item [pokemon]="pokemon" [show]="show"></app-pokemon-list-item>
        </div>
      </div>
      <div *ngIf="!show">
        <app-pagination></app-pagination>
      </div>
    </div>
    `,
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit{
  @Input() pokemons!: Pokemon[] ;
  @Input() show!: boolean;


  constructor(private readonly pokemonService: PokemonService) {
  }

  ngOnInit()  {
    this.pokemonService.fetchPokemons()
  }


}
