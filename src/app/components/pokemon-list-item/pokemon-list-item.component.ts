import {Component, Input} from '@angular/core'
import {Pokemon} from "../../models/pokemon.model";
import {TrainerService} from "../../services/trainer.service";
import {SessionService} from "../../services/session.service";
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector:'app-pokemon-list-item',
  template:
    `<div *ngIf="pokemon" class="card" (click)="onPokemonClick()">
      <img class="pokemon-image" [src]="pokemon.imageURL" [alt]="pokemon.name" />
      <div class="pokemon-name">{{pokemon.name}}</div>
      <span class="material-icons delete-button" *ngIf="show === true" (click)="onPokemonDelete()">
          highlight_off
        </span>
      <div [@catch]="isOpen ? 'open' : 'closed'" (@catch.done)="animationDone()" class="img"></div>
    </div>
    `,
  styleUrls: ['./pokemon-list-item.component.css'],
  animations: [
    trigger('catch', [
      // ...
      state('open', style({
        height: '0px',
        opacity: 1,
      })),
      state('closed', style({
        height: '20rem',
        opacity: 1,
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.8s')
      ]),
    ]),


  ],
})
export class PokemonListItemComponent {
  @Input() pokemon!: Pokemon;
  @Input() show!: boolean;

  constructor(
    private readonly trainerService: TrainerService,
    private readonly sessionService: SessionService
  ) {}

  isOpen = true;

  animationDone() {
    this.isOpen = true
  }

  public onPokemonClick(): void {
    let includes = false;
    for (const pokemon of this.sessionService.trainer!.pokemon) {
      if(pokemon.name === this.pokemon.name){
        includes = true;
      }
    }
    if(!includes) {
      this.trainerService.collectPokemon(this.sessionService.trainer!.id, this.pokemon);
      this.isOpen = !this.isOpen;
    }
  }

  public onPokemonDelete(): void{
    this.trainerService.removePokemon(this.sessionService.trainer!.id, this.pokemon)
  }


}

