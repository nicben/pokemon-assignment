
import {Component, Input} from '@angular/core'
import {PokemonService} from "../../services/pokemon.service";
import {Pokemon} from "../../models/pokemon.model";
import {SessionService} from "../../services/session.service";
import {Router} from "@angular/router";
import {Trainer} from "../../models/trainer.model";

@Component({
  selector:'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  @Input() pokemons: Pokemon[] | undefined;

  constructor(private readonly router: Router,
              private readonly pokemonService: PokemonService,
              private readonly sessionService: SessionService) {
  }

  onCatalogueClick(): void {
    console.log("onclickCatalogue")
    this.router.navigate([`catalogue`])
  }

  onTrainerClick(): void {
    console.log("onclickTrainer")
    this.router.navigate([`trainer`])
  }

  onLogoutClick(): void {
    console.log("onclickLogout")
    this.sessionService.logout()
    this.router.navigate([`login`])
  }

  trainerLoggedIn(): Trainer{
    return this.sessionService.trainer!
  }
}
