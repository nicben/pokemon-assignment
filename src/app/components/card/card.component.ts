import {Component} from "@angular/core";

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div class="body">
        <ng-content></ng-content>
      </div>
      <div class="footer"></div>
    </div>
  `,
  styleUrls: ['card.component.css']
})
export class CardComponent{

}
